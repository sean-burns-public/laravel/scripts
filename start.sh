#!/bin/bash

# Get the directory of the current script
SCRIPT_DIR="$(dirname "$(realpath "$0")")"

# Get the parent directory of the script's directory
PARENT_DIR="$(dirname "$SCRIPT_DIR")"

# Include the script for adding shell aliases
source $SCRIPT_DIR/add_alias.sh

# ANSI color codes
RED_BACKGROUND="\033[41m"
GREEN_BACKGROUND="\033[42m"
ORANGE_BACKGROUND="\033[48;5;208m"
WHITE_TEXT="\033[1;37m"
BLACK_TEXT="\033[30m"
NO_COLOR="\033[0m"

# Check if this is the root of a Laravel project
if [ ! -f "artisan" ] || [ ! -f "composer.json" ]; then
    echo -e "${RED_BACKGROUND}${WHITE_TEXT}Error: This script must be run from the root of a Laravel project.${NO_COLOR}"
    exit 1
fi

# Check if Docker Daemon is running
if docker info > /dev/null 2>&1; then
    echo "Docker Daemon is running."
else
    echo -e "${RED_BACKGROUND}${WHITE_TEXT}Error: Docker Daemon is not running.${NO_COLOR}"
    exit 1
fi


#####################################
# Deleting the vendor folder...
#####################################

echo "Deleting existing vendor folder (if any)..."
rm -rf ./vendor


#####################################
# Add some aliases for convenience...
#####################################

if [[ "$1" == "ci-cd" ]]; then
    echo "No aliases will be created as we're running in a CI/CD pipeline."
else
    # Sail:
    add_alias "sail" "bash vendor/bin/sail"

    # Clear All (both terminal and scrollback):
    add_alias "ca" "clear && printf '\e[3J'"

    # Open the project commands/shortcuts menu
    add_alias "menu" "./project_scripts/menu.sh"
fi

# Make the .env file from the .env.example template
cp -f .env.example .env

# As the git commit SHA is only updated when the server is deployed, when started locally, add a dummy placeholder to avoid stale/confusing SHAs
echo "GIT_COMMIT_SHA=LOCAL_WORKING_COPY" >> .env

# Use a temporary Docker container to run Composer install with additional options
echo "Running Composer install using a temporary Docker container..."
docker run --rm -v $(pwd):/app composer:2.8.2 sh -c "php -v && composer install --no-interaction --prefer-dist --optimize-autoloader --ignore-platform-reqs"

# Start Laravel Sail (Docker containers)
echo "Starting Laravel Sail..."
./vendor/bin/sail up -d

# Collect MySQL credentials from `.env` file
MYSQL_PORT=$(grep DB_PORT .env | cut -d '=' -f2 | tr -d '[:space:]')
MYSQL_USERNAME=$(grep DB_USERNAME .env | cut -d '=' -f2 | tr -d '[:space:]')
MYSQL_PASSWORD=$(grep DB_PASSWORD .env | cut -d '=' -f2 | tr -d '[:space:]')

if [[ "$1" == "ci-cd" ]]; then
  echo "We can't easily test for MySQL's port being open, so we will just wait for a short time..."
  sleep 10
  echo -e "\nFinished waiting."
else
  # Wait for MySQL's port to be open
  echo "Waiting for MySQL to open its port..."
  until nc -z localhost $MYSQL_PORT; do
    echo -n "." # Print a dot to show a waiting status
    sleep 1
  done
  echo -e "\nMySQL port is open."
fi

# Wait for MySQL to be ready
echo "Waiting for MySQL to be ready to accept queries..."
until ./vendor/bin/sail exec mysql mysql -u"$MYSQL_USERNAME" -p"$MYSQL_PASSWORD" -e "SELECT 1;" > /dev/null 2>&1; do
  echo -n "." # Print a dot to show a waiting status
  sleep 1
done
echo "MySQL is ready to accept queries."

# If it doesn't already exist, create a symbolic link from "public/storage" to "storage/app/public"
if [ ! -L "public/storage" ]; then
    echo "Creating symbolic link for storage..."
    ./vendor/bin/sail artisan storage:link
else
    echo "Symbolic link for storage already exists. Skipping..."
fi

# Clearing various caches and re-optimizing
echo "Clearing caches and re-optimizing..."
./vendor/bin/sail artisan cache:clear
./vendor/bin/sail artisan route:clear
./vendor/bin/sail artisan config:clear
./vendor/bin/sail artisan view:clear

# Run database migrations inside the Docker container, dropping all tables first and then seeding
echo "Running database migrations (fresh) and seeding..."
./vendor/bin/sail artisan migrate:fresh --seed

# Optional: If your Laravel project includes a front-end that requires compiling
echo "Installing Node modules..."
./vendor/bin/sail npm install

echo "Building assets for development..."

# Define the complete command(s) including changing directory
SAIL_NPM_RUN_CMD="cd '$PARENT_DIR' && ./vendor/bin/sail npm run"
SAIL_NPM_RUN_DEV_CMD="$SAIL_NPM_RUN_CMD dev"
SAIL_NPM_RUN_BUILD_CMD="$SAIL_NPM_RUN_CMD build"

if [[ "$1" == "ci-cd" ]]; then
  echo "Running in a CI/CD pipeline."

  # Execute the command in the background and surpress (the otherwise intermingled) console output
  bash -c "$SAIL_NPM_RUN_BUILD_CMD" > /dev/null 2>&1 &

elif [[ "$OSTYPE" == "darwin"* ]]; then
    echo "Running on macOS."

    # macOS specific commands:

    # Running a multi-line AppleScript using the absolute path
    osascript <<EOF
tell application "Terminal"
    do script "$SAIL_NPM_RUN_DEV_CMD"
    activate
end tell
EOF

elif [ -n "$WSL_DISTRO_NAME" ]; then
    echo "Running on Windows (inside WSL2); script will assume this is a Ubuntu distribution."

    # Ubuntu in Windows WSL2 specific commands:

    cd /mnt/c && cmd.exe /c start wsl -d $WSL_DISTRO_NAME -e bash -c "${SAIL_NPM_RUN_DEV_CMD} && exec bash" &
    cd $PARENT_DIR

else

    echo -e "${RED_BACKGROUND}${WHITE_TEXT}Cannot start separate terminal window as not running on macOS or WSL2/Ubuntu...${NO_COLOR}"
    echo "Please start a new terminal window and execute commands similar to the following:"
    echo "cd ~/repositories/app"
    echo "./vendor/bin/sail npm run dev"

fi

echo -e "${GREEN_BACKGROUND}${WHITE_TEXT}Laravel application is up and running in development mode!${NO_COLOR}"

if [[ "$1" == "demo" ]]; then
    echo -e "${ORANGE_BACKGROUND}${BLACK_TEXT}"
    echo -e "                                            "
    echo -e " NOTE: Seeding the database with demo data! "
    echo -e "                                            "
    echo -e "${NO_COLOR}"
    ./vendor/bin/sail artisan db:seed --class=DemoSeeder
fi
