#!/bin/bash

# ANSI color codes
RED_BACKGROUND="\033[41m"
GREEN_BACKGROUND="\033[42m"
WHITE_TEXT="\033[1;37m"
NO_COLOR="\033[0m"

# Check if this is the root of a Laravel project
if [ ! -f "artisan" ] || [ ! -f "composer.json" ]; then
    echo -e "${RED_BACKGROUND}${WHITE_TEXT}Error: This script must be run from the root of a Laravel project.${NO_COLOR}"
    exit 1
fi

# Start Laravel Sail (Docker containers) if not already running
echo "Ensuring Laravel Sail is up..."
./vendor/bin/sail up -d

# Clearing caches and config
echo "Clearing caches and re-optimizing..."
./vendor/bin/sail artisan cache:clear
./vendor/bin/sail artisan route:clear
./vendor/bin/sail artisan config:clear
./vendor/bin/sail artisan view:clear

# Run new migrations if there are any
echo "Running new database migrations..."
./vendor/bin/sail artisan migrate

# Optional: Rebuild front-end assets
# echo "Rebuilding front-end assets..."
# ./vendor/bin/sail npm run dev

echo -e "${GREEN_BACKGROUND}${WHITE_TEXT}Development environment is updated and ready!${NO_COLOR}"
