#!/bin/sh

# ANSI color codes using tput
RED_BACKGROUND="$(tput setab 1)"
ORANGE_BACKGROUND="$(tput setab 208)"
GREEN_BACKGROUND="$(tput setab 2)"
WHITE_TEXT="$(tput setaf 7)"
BLACK_TEXT="$(tput setaf 0)"
NO_COLOR="$(tput sgr0)"

# Get the user's default shell
user_shell="$(basename "$SHELL")"

# Define the file and commands to be added based on the user's shell
if [ "$user_shell" = "bash" ]; then
  profile_file="$HOME/.bashrc"
elif [ "$user_shell" = "zsh" ]; then
  profile_file="$HOME/.zshrc"
else
  printf "%sError: Unsupported shell: %s%s\n" "$RED_BACKGROUND$WHITE_TEXT" "$user_shell" "$NO_COLOR"
  exit 1
fi

# Check if the 'eval $(ssh-agent -s)' command is already present in the shell's profile file
if grep -q 'eval $(ssh-agent -s)' "$profile_file"; then
  printf "%sWarning: 'eval \$(ssh-agent -s)' already present in %s. \nSSH commands not added.%s\n" "$ORANGE_BACKGROUND$BLACK_TEXT" "$profile_file" "$NO_COLOR"
  exit 1
fi

# Prompt the user for the SSH key filename
read -p "Enter the SSH key filename (default: id_rsa): " ssh_key_filename
ssh_key_filename=${ssh_key_filename:-id_rsa} # Set to 'id_rsa' if empty

ssh_commands="

alias sssh=\"start_ssh_agent\"
start_ssh_agent() {
  if [ -z \"\$SSH_AUTH_SOCK\" ]; then
    eval \$(ssh-agent -s)
  fi

  # Add your SSH keys here if needed
  ssh-add ~/.ssh/$ssh_key_filename
}
"


# Append the SSH commands to the profile file
printf "%s" "$ssh_commands" >> "$profile_file"
printf "%sSSH commands added to %s.%s\n" "$GREEN_BACKGROUND$WHITE_TEXT" "$profile_file" "$NO_COLOR"
printf "%sPlease restart the shell/terminal so that the commands take effect.%s\n" "$ORANGE_BACKGROUND$WHITE_TEXT" "$NO_COLOR"
printf "%sThen use the command sssh to Start SSH in each new session.%s\n" "$ORANGE_BACKGROUND$WHITE_TEXT" "$NO_COLOR"
