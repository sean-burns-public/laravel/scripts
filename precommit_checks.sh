#!/bin/bash

# ANSI color codes
RED_BACKGROUND="\033[41m"
GREEN_BACKGROUND="\033[42m"
WHITE_TEXT="\033[1;37m"
NO_COLOR="\033[0m"

# Check if this is the root of a Laravel project
if [ ! -f "artisan" ] || [ ! -f "composer.json" ]; then
    echo -e "${RED_BACKGROUND}${WHITE_TEXT}Error: This script must be run from the root of a Laravel project.${NO_COLOR}"
    exit 1
fi

# Spell Check
# TBD

# Check AsciiDoc Format
# TBD

# Linting
echo "If any issues are identified during Linting, consider executing 'sail pint' to auto-resolve them."
echo "Linting project..."
./vendor/bin/sail pint --test

# Static Analysis
echo "Performing Static Analysis..."
./vendor/bin/sail exec laravel.test ./vendor/bin/phpstan analyse

# Automated Testing
echo "Performing automated tests..."
./vendor/bin/sail test
