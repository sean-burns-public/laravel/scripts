# ANSI color codes
RED_BACKGROUND="\033[41m"
GREEN_BACKGROUND="\033[42m"
WHITE_TEXT="\033[1;37m"
NO_COLOR="\033[0m"

# Function to add an alias
add_alias() {
    local ALIAS_NAME="$1"
    local ALIAS_COMMAND="$2"
    local SHELL_CONFIG

    # Determine which shell configuration file to update
    if [ "$SHELL" = "/bin/zsh" ]; then
        SHELL_CONFIG="$HOME/.zshrc"
    elif [ "$SHELL" = "/bin/bash" ]; then
        SHELL_CONFIG="$HOME/.bashrc"
    else
        echo -e "${RED_BACKGROUND}${WHITE_TEXT}Unsupported shell. Only bash and zsh are supported.${NO_COLOR}"
        exit 1
    fi

    # Check if the alias already exists in the shell configuration file
    if grep -q "alias $ALIAS_NAME=" "$SHELL_CONFIG"; then
        echo -e "${GREEN_BACKGROUND}${WHITE_TEXT}Alias $ALIAS_NAME already exists in $SHELL_CONFIG${NO_COLOR}"
    else
        # Add the alias to the shell configuration file
        if [ -n "$(tail -c 1 "$SHELL_CONFIG")" ]; then
            echo "" >> "$SHELL_CONFIG"
        fi
        echo "alias $ALIAS_NAME=\"$ALIAS_COMMAND\"" >> "$SHELL_CONFIG"
        echo -e "${GREEN_BACKGROUND}${WHITE_TEXT}Alias $ALIAS_NAME added to $SHELL_CONFIG${NO_COLOR}"
        # Note: Although the alias has been added for the current session, said session doesn't persist after this script.
        echo -e "${RED_BACKGROUND}${WHITE_TEXT}Important: Either execute 'source $SHELL_CONFIG' or restart your terminal to enable the $ALIAS_NAME alias.${NO_COLOR}"
    fi
}

# Example usage (commented out):
# add_alias "sail" "bash vendor/bin/sail"
